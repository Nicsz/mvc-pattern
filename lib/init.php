<?php
  function dispatch($uri) {

//    var_dump('YES');

    //Defaults
    $controller = 'index';
    $action = 'index';
    $params = array();

    //Put to lower
    $uri = strtolower($uri);

    //1. Clean
    $uri = trim($uri, '/');

    if ($uri) {
      //3. Explode
      $path_parts = explode('/', $uri);

      if ($path_parts) {
        //4. Get controller
        $controller = array_shift($path_parts);
      }

      if ($path_parts) {
        //5. Get action
        $action = array_shift($path_parts);
      }

      //6. Get parameters
      $params = $path_parts;
    }

   return compact('controller', 'action', 'params');
  }

  function runApp($uri) {
    //1. Get controller and action names
    $dispatch_data = dispatch($uri);

    //2. Include controller' file
    $controller_name = $dispatch_data['controller'];
    $controller_file = "controllers/{$controller_name}.controller.php";

    if (!file_exists($controller_file)) {
//      header("HTTP/1.1 404 Not Found");
      include "views/default.php";
      exit();
    }

    include $controller_file;


    //3. Run controller' function(action)
    $action = $dispatch_data['action'];

    if (!function_exists($action)) {
//      header("HTTP/1.1 404 Not Found");
      include "views/default.php";
      exit();
    }

    //This is FUNCTION CALL !!!
    $result = $action();

    //4. Put data into view (.html)
    ob_start();
    include "views/{$controller_name}/{$action}.php";
    $content = ob_get_clean();

    //5. Echo data to end-user
    include "views/default.php";
  }