<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="">
  <meta name="description" content="">

  <title>Minimax HTML5 Free Template</title>
  <!--

  Template 2080 Minimax

  http://www.tooplate.com/view/2080-minimax

  -->
  <!-- stylesheet css -->
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/font-awesome.min.css">
  <link rel="stylesheet" href="/css/nivo-lightbox.css">
  <link rel="stylesheet" href="/css/nivo_themes/default/default.css">
  <link rel="stylesheet" href="/css/style.css">
  <!-- google web font css -->
  <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,600,700' rel='stylesheet' type='text/css'>

</head>
<body>
  <div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon icon-bar"></span>
          <span class="icon icon-bar"></span>
          <span class="icon icon-bar"></span>
        </button>
        <a href="#home" class="navbar-brand smoothScroll">Minimax</a>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="/products" class="smoothScroll">Catalog</a></li>
          <li><a href="/products/search" class="smoothScroll">Search</a></li>
          <li><a href="/products/about" class="smoothScroll">About</a></li>
          <li><a href="/contacts" class="smoothScroll">Contacts</a></li>
        </ul>
      </div>
    </div>
  </div>

  <nav>
    <a href="/products">Catalog</a>
    <a href="/products/search">Search</a>
    <a href="/products/about">About</a>
    <a href="/contacts">Contacts</a>
  </nav>

  <div id="about">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <h2>Minimax Story</h2>
        </div>
        <div class="col-md-12 col-sm-12">
          <?php echo $content; ?>
        </div>
      </div>
    </div>
  </div>


  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <h2>Our Office</h2>
          <p>101 Clean Street, California, CA 10110</p>
          <p>Email: <span>hello@company.com</span></p>
          <p>Phone: <span>010-020-0340</span></p>
        </div>
        <div class="col-md-6 col-sm-6">
          <h2>Social Us</h2>
          <ul class="social-icons">
            <li><a href="#" class="fa fa-facebook"></a></li>
            <li><a href="#" class="fa fa-twitter"></a></li>
            <li><a href="#" class="fa fa-google-plus"></a></li>
            <li><a href="#" class="fa fa-dribbble"></a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
</body>
</html>