<form class="form" action="">
  <h2>Contact Us</h2>
  <label for="name">Name:</label>
  <input type="text" name="name" placeholder="Write your name">
  <label for="email">Email:</label>
  <input type="email" name="email" placeholder="Let us know">
  <label for="message">Message:</label>
  <input type="text" name="message" placeholder="What">
  <button type="submit">Send Message</button>
  <div>
    <span class="fa fa-phone"></span>001 1023 567
    <span class="fa fa-envelope-o"></span> contact
  </div>
</form>